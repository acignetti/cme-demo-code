package com.cme.helloworld;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private CalendarView calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        calendar = (CalendarView) findViewById(R.id.calendarView);

        if (calendar != null) {
            calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                @Override
                public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                    String finalString = "User selected: " + Integer.toString(year) + "-"
                            + Integer.toString(month + 1) + "-" + Integer.toString(dayOfMonth);

                    Toast.makeText(getApplicationContext(), finalString, Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, R.string.on_resume_text, Toast.LENGTH_SHORT).show();
        Log.d(this.getClass().toString(), "Application resumed");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, R.string.on_pause_text, Toast.LENGTH_SHORT).show();
        Log.d(this.getClass().toString(), "Application paused");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this, R.string.on_stop_message, Toast.LENGTH_SHORT).show();
        Log.d(this.getClass().toString(), "Application stopped");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, R.string.on_destroy_message, Toast.LENGTH_SHORT).show();
        Log.d(this.getClass().toString(), "Application destroyed");
    }

    public void showMessage(View view) {
        Intent newView = new Intent(this, SecondActivity.class);
        EditText userInput = (EditText) findViewById(R.id.user_input);

        newView.putExtra("user_input", userInput.getText().toString());

        startActivity(newView);
    }
}
