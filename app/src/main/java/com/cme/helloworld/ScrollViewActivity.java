package com.cme.helloworld;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ScrollViewActivity extends AppCompatActivity {

    private LinearLayout myView;
    private Button nextButton;
    private int index = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroll_view);

        myView = (LinearLayout) findViewById(R.id.elem_list);
        nextButton = (Button) findViewById(R.id.next);

        nextButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent newView = new Intent(getApplicationContext(), ListViewActivity.class);

                startActivity(newView);

                return true;
            }
        });
    }

    public void addNewElement(View v) {
        TextView current = new TextView(this);
        current.setText("Element " + index++);

        myView.addView(current);
    }

}
