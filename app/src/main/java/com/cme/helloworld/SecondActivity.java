package com.cme.helloworld;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        String information = (String) getIntent().getSerializableExtra("user_input");

        if (information.isEmpty()) {
            ((TextView) findViewById(R.id.user_input_text)).setText(R.string.no_input);
        } else {
            ((TextView) findViewById(R.id.user_input_text)).setText(information);
        }

    }

    public void backButton(View v) {
        onBackPressed();
    }

    public void showListViewExample(View v) {
        startActivity(new Intent(this, ScrollViewActivity.class));
    }

}
