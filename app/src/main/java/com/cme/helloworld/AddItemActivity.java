package com.cme.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.cme.helloworld.item.Item;
import com.cme.helloworld.item.ItemFactory;

public class AddItemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
    }

    public void addElement(View v) {
        EditText itemDescription = (EditText) findViewById(R.id.elem_description);
        EditText itemValue = (EditText) findViewById(R.id.elem_value);

        ItemFactory.getInstance(this).addItem(new Item(
            itemDescription.getText().toString(),
            Double.parseDouble(itemValue.getText().toString())
        ));

        onBackPressed();
    }
}
