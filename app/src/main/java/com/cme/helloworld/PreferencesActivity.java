package com.cme.helloworld;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class PreferencesActivity extends AppCompatActivity {

    public static final String PREFERENCES_NAME = "com.cme.helloworld.PREFERENCES";
    public static final String USE_WIFI = "com.cme.helloworld.USE_WIFI";
    public static final String USE_GPS = "com.cme.helloworld.USE_GPS";
    public static final String USE_NETWORK = "com.cme.helloworld.USE_NETWORK";
    public static final String USERNAME = "com.cme.helloworld.USERNAME";
    public static final String DISTANCE = "com.cme.helloworld.DISTANCE";

    private static SharedPreferences preferences;

    private CheckBox useWifi;
    private CheckBox useGps;
    private CheckBox useNetwork;
    private EditText userName;
    private EditText distance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);

        useWifi = (CheckBox) findViewById(R.id.pref_wifi);
        useGps = (CheckBox) findViewById(R.id.pref_gps);
        useNetwork = (CheckBox) findViewById(R.id.pref_network);
        userName = (EditText) findViewById(R.id.pref_user);
        distance = (EditText) findViewById(R.id.pref_distance);
        preferences = getSharedPreferences(PREFERENCES_NAME, 0);

        useWifi.setChecked(preferences.getBoolean(USE_WIFI, false));
        useGps.setChecked(preferences.getBoolean(USE_GPS, false));
        useNetwork.setChecked(preferences.getBoolean(USE_NETWORK, false));
        userName.setText(preferences.getString(USERNAME, ""));
        distance.setText(String.valueOf(preferences.getFloat(DISTANCE, 0)));


        findViewById(R.id.save_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        savePreferences();
        super.onBackPressed();
    }


    private void savePreferences() {
        SharedPreferences.Editor editor = preferences.edit();

        editor.putBoolean(USE_WIFI, useWifi.isChecked());
        editor.putBoolean(USE_GPS, useGps.isChecked());
        editor.putBoolean(USE_NETWORK, useNetwork.isChecked());
        editor.putString(USERNAME, userName.getText().toString());
        editor.putFloat(DISTANCE, Float.valueOf(distance.getText().toString()));

        editor.commit();
    }
}
