package com.cme.helloworld.item;

import android.content.Context;

import com.cme.helloworld.database.DatabaseHelper;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

public class ItemFactory {

    private static ItemFactory instance;
    private Dao<Item, Integer> itemDao;

    private ItemFactory(Context context) {
        OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(context, DatabaseHelper.class);

        try {
            itemDao = helper.getDao(Item.class);
        } catch (SQLException e) {
            // Boo hoo
        }
    }

    public static ItemFactory getInstance(Context context) {
        if (instance == null) {
            instance = new ItemFactory(context);
        }

        return instance;
    }

    public List<Item> getItemList() throws SQLException {
        return itemDao.queryForAll();
    }

    public boolean addItem(Item newItem) {
        try {
            itemDao.create(newItem);
        } catch (SQLException e) {
            return false;
        }

        return true;
    }

    public boolean removeItem(Item toDelete) {
        try {
            itemDao.delete(toDelete);
        } catch (SQLException e) {
            return false;
        }

        return true;
    }
}
