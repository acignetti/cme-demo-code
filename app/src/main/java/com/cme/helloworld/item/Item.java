package com.cme.helloworld.item;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = "items")
public class Item implements Serializable {

    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
    private int myId;

    @DatabaseField
    private String description;

    @DatabaseField
    private double value;

    public Item() {}

    public Item(String description, double value) {
        myId = 0;
        this.description = description;
        this.value = value;
    }

    public int getMyId() {
        return myId;
    }

    public void setMyId(int myId) {
        this.myId = myId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
