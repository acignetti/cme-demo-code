package com.cme.helloworld.item;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cme.helloworld.R;

import java.util.List;

public class ItemAdapter extends BaseAdapter {

    private List<Item> items;

    public ItemAdapter(List<Item> items) {
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Item getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getMyId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View currentView;

        if (convertView == null) {
            currentView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_item, parent, false);
        } else {
            currentView = convertView;
        }

        Item item = getItem(position);

        TextView id = (TextView) currentView.findViewById(R.id.elem_id);
        TextView description = (TextView) currentView.findViewById(R.id.elem_description);
        TextView value = (TextView) currentView.findViewById(R.id.elem_value);

        id.setText(String.valueOf(item.getMyId()));
        description.setText(item.getDescription());
        value.setText(String.valueOf(item.getValue()));

        return currentView;
    }

    public void setNewElements(List<Item> newElements) {
        items = newElements;
    }
}
