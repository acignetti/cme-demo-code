package com.cme.helloworld;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.cme.helloworld.item.Item;
import com.cme.helloworld.item.ItemAdapter;
import com.cme.helloworld.item.ItemFactory;

import java.sql.SQLException;

public class ListViewActivity extends AppCompatActivity {

    private ListView itemList;
    private ItemAdapter adapter;
    private ItemFactory itemFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        itemList = (ListView) findViewById(R.id.elem_list);
        itemFactory = ItemFactory.getInstance(this);

        try {
            adapter = new ItemAdapter(itemFactory.getItemList());
            itemList.setAdapter(adapter);
        } catch (Exception none) {
            Log.e(ListViewActivity.class.toString(), "Something bad happened");
        }

        itemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Item selected = (Item) parent.getItemAtPosition(position);

                Intent description = new Intent(getApplicationContext(), DescriptionActivity.class);

                description.putExtra("item", selected);

                startActivity(description);
            }
        });
    }

    @Override
    protected void onResume() {
        try {
            adapter.setNewElements(itemFactory.getItemList());
            adapter.notifyDataSetChanged();
        } catch (SQLException e) {
            Log.e(ListViewActivity.class.toString(), "Something bad happened");
        }

        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_item:
                Intent creator = new Intent(this, AddItemActivity.class);
                startActivity(creator);
                return true;
            case R.id.preferences:
                Intent prefs = new Intent(this, PreferencesActivity.class);
                startActivity(prefs);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
