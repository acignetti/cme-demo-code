package com.cme.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.cme.helloworld.item.Item;
import com.cme.helloworld.item.ItemFactory;

public class DescriptionActivity extends AppCompatActivity {

    private Item showing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);

        showing = (Item) getIntent().getSerializableExtra("item");

        ((TextView) findViewById(R.id.elem_description)).setText(showing.getDescription());
        ((TextView) findViewById(R.id.elem_value)).setText(String.valueOf(showing.getValue()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.item_action_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.remove_item:
                ItemFactory.getInstance(this).removeItem(showing);
                onBackPressed();
                return true;
            case R.id.edit_item:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
